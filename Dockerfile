FROM registry.access.redhat.com/ubi8/python-39 AS builder
USER root
WORKDIR /build
COPY src /build
RUN dnf -y install upx
RUN pip3 install -r requirements.txt
RUN pyinstaller -s -F example.py
RUN mkdir -p /target/hcs-company.com
RUN mkdir -m 1777 /target/tmp
RUN staticx /build/dist/example /target/hcs-company.com/example; chmod 755 /target/hcs-company.com/example


FROM scratch
WORKDIR /
USER root
COPY --from=builder /target /
CMD ["/hcs-company.com/example"]
EXPOSE 8080/tcp
USER 1001
