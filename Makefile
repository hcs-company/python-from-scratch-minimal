.PHONY: push build run

IMAGETAG := minimal-python-example

push: build
	@podman push $(IMAGETAG) --tls-verify=false

build:
	@podman build --rm -t $(IMAGETAG) .

run: build
	@podman run -it -p 8080:8080 --rm ${IMAGETAG}

