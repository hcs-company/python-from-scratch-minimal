import logging
from http.server import BaseHTTPRequestHandler, HTTPServer

class http_handler(BaseHTTPRequestHandler):
    def do_GET(self):
        logging.info(f'GET request received: {self.path}')
        self.send_response(200)
        self.send_header('Content-Type', 'text/html')
        self.end_headers()
        self.wfile.write(bytes(f'<body>Dit is een basis http server</br>Je vroeg om {self.path}</body>\n', 'utf-8'))


class MyHTTPServer(object):
    def __init__(self, port=8080):
        self.port = port
        logging.info(f'Starting MyHTTPServer on port {self.port}')
        self.httpd = HTTPServer(('0.0.0.0', self.port), http_handler)
        logging.info(f'MyHTTPServer started')

    def run(self):
        try:
            self.httpd.serve_forever()
        except KeyboardInterrupt:
            pass

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.info('Starting up...')
    http_server = MyHTTPServer()
    http_server.run()
    logging.info('Exiting')
